# coding=utf8
# the above tag defines encoding for this document and is for Python 2.x compatibility

import re

regex1 = r"(\(footprint \"Passives:)via_diff_pair(\".*\n *\(tstamp.*\n *\(at.*\n *\(attr.*\n *\(fp_text.*\n.*\n.*\n *\)\n *\(fp_text value \")via_diff_pair(\".*\n.*\n.*\n *\)\n *\(pad \")~(\".*\n.*\n *\(pad \")~(\".*\n.*\n *\(pad \")~(\".*\n.*\n *\(pad \")~(.*)"

regex2 = r"(\(footprint \"Passives:)via_diff_pair(\".*\n *\(tstamp.*\n *\(at.*\n *\(attr.*\n *\(fp_text.*\n.*\n.*\n *\)\n *\(fp_text value \")via_diff_pair(\".*\n.*\n.*\n *\)\n *\(pad \")`(\".*\n.*\n *\(pad \")~(\".*\n.*\n *\(pad \")~(\".*\n.*\n *\(pad \")~(.*)"



with open("../carrier.kicad_pcb") as basefile:
    file_contents = basefile.read()

subst1 = "\\1via_diff_pair_v2\\2via_diff_pair_v2\\3G\\4P\\5N\\6G\\7"
subst2 = "\\1via_diff_pair_v2\\2via_diff_pair_v2\\3P\\4G\\5N\\6G\\7"

# You can manually specify the number of replacements by changing the 4th argument
result1 = re.sub(regex1, subst1, file_contents, 0, re.MULTILINE)
result2 = re.sub(regex2, subst2, result1, 0, re.MULTILINE)

if result2:
    print (result2)

# Note: for Python 2.7 compatibility, use ur"" to prefix the regex and u"" to prefix the test string and substitution.
